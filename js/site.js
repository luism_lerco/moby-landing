!(function($){
    // Initi AOS
  AOS.init({
    duration: 1000,
    easing: "ease-in-out",
    once: true,
    mirror: false
  });
})(jQuery);

/* Smooth Scroll */
var scroll = new SmoothScroll('a[href*="#"]');